import "@testing-library/jest-dom";
import { render, fireEvent, screen } from "@testing-library/react";
import App from "../App";

beforeAll(() => {
  const localStorageMock = (function () {
    // Defines an object with string keys and string values
    let store: Record<string, string> = {};
    return {
      getItem: function (key: string) {
        return store[key] || null;
      },
      setItem: function (key: string, value: string) {
        store[key] = value.toString();
      },
      clear: function () {
        store = {};
      },
    };
  })();

  Object.defineProperty(window, "localStorage", { value: localStorageMock });
});

describe("App Component", () => {
  test("renders PassengerForm initially", () => {
    render(<App />);
    expect(screen.getByText("PASSENGER DETAILS")).toBeInTheDocument();
  });

  test('navigates to ConfirmationPage on "Next"', () => {
    render(<App />);
    fireEvent.click(screen.getByText("Next"));
    expect(screen.getByText("Confirmation Page")).toBeInTheDocument();
  });
});
