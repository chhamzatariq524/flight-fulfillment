import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import ConfirmationPage from "../components/ConfirmationPage";

describe("ConfirmationPage Component", () => {
  test("displays changes made", () => {
    const { getByText, getAllByText } = render(
      <ConfirmationPage
        passengers={{
          0: {
            firstName: "John",
            lastName: "Doe",
            dateOfBirth: "2003-08-31",
            gender: "MALE",
            title: "MR",
          },
        }}
        initialPassengers={{
          0: {
            firstName: "Jane",
            lastName: "Doe",
            dateOfBirth: "2003-08-31",
            gender: "FEMALE",
            title: "MRS",
          },
        }}
        onChangesUpdate={() => {}}
      />
    );

    expect(getAllByText(/First Name/).length).toBeGreaterThan(0);
    expect(getByText("Jane → John")).toBeInTheDocument();

    expect(getByText("MRS → MR")).toBeInTheDocument();
  });
});
