import "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";
import PassengerForm from "../components/PassengerForm";

describe("PassengerForm Component", () => {
  const mockOnPassengerChange = jest.fn();
  const mockOnChangesMade = jest.fn();

  test("handles input change", () => {
    const { getByLabelText } = render(
      <PassengerForm
        passengers={{
          0: {
            firstName: "Jane",
            lastName: "Doe",
            dateOfBirth: "2003-08-31",
            gender: "FEMALE",
            title: "MRS",
          },
        }}
        onPassengerChange={mockOnPassengerChange}
        onChangesMade={mockOnChangesMade}
      />
    );

    fireEvent.change(getByLabelText("First Name"), {
      target: { value: "John" },
    });
    expect(mockOnPassengerChange).toHaveBeenCalledWith(
      0,
      expect.objectContaining({
        firstName: "John",
      })
    );
  });
});
