import React from "react";
import Button from "@mui/material/Button";

const CustomButton = ({ onClick, disabled, children, sx }) => {
  return (
    <Button
      variant="contained"
      onClick={onClick}
      disabled={disabled}
      sx={{
        ...sx,
        marginLeft: sx?.marginLeft || "20vw",
        marginRight: sx?.marginRight || "20vw",
        bgcolor: "#1A115A",
        borderRadius: "0px",
        width: sx?.width || "20vw",
        marginTop: sx?.marginTop || "5vh",
      }}
    >
      {children}
    </Button>
  );
};

export default CustomButton;
