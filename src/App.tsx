import React, { useState, useEffect } from "react";
import { Stepper, Step, StepLabel } from "@mui/material";
import PassengerForm from "./components/PassengerForm";
import ConfirmationPage from "./components/ConfirmationPage";
import HeaderPage from "./components/Header";
import CustomButton from "./composable/CustomButton";
import "./App.css";

const steps = ["Passenger Details", "Confirmation"];

const App: React.FC = () => {
  const [passengers, setPassengers] = useState<{
    [key: string]: {
      title: string;
      gender: string;
      firstName: string;
      lastName: string;
      dateOfBirth: string;
    };
  }>({});

  const [initialPassengers, setInitialPassengers] = useState<{
    [key: string]: {
      title: string;
      gender: string;
      firstName: string;
      lastName: string;
      dateOfBirth: string;
    };
  }>({
    0: {
      title: "MRS",
      gender: "FEMALE",
      firstName: "Jane",
      lastName: "Doe",
      dateOfBirth: "2003-08-31",
    },
    1: {
      title: "MR",
      gender: "MALE",
      firstName: "John",
      lastName: "Doe",
      dateOfBirth: "2001-04-12",
    },
  });

  const [activeStep, setActiveStep] = useState(0);
  const [changesMade, setChangesMade] = useState(0);
  const [passengerChanges, setPassengerChanges] = useState<
    Array<{
      passengerIndex: number;
      field: string;
      newValue: string;
    }>
  >([]);

  useEffect(() => {
    // Check if there are passengers in localStorage
    const storedPassengers = localStorage.getItem("passengers");
    if (storedPassengers) {
      const parsedPassengers = JSON.parse(storedPassengers);
      setPassengers(parsedPassengers);
      setInitialPassengers(parsedPassengers);
    } else {
      // Set initial passengers if not found in localStorage
      setPassengers(initialPassengers);
      localStorage.setItem("passengers", JSON.stringify(initialPassengers));
    }
  }, []);

  // Next Button handler
  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  // Back / Edit Button handler
  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
    setPassengers({ ...initialPassengers });

    setChangesMade(0);
    setPassengerChanges([]);
  };

  // Passenger change handler
  const handlePassengerChange = (index: number, passenger: any) => {
    setPassengers({
      ...passengers,
      [index]: passenger,
    });
  };

  // Submit button handler
  const handleSaveChanges = () => {
    console.log(passengerChanges);

    passengerChanges.forEach((change) => {
      console.log("Changes:", change);
      const { passengerIndex, field, newValue } = change;
      (initialPassengers[passengerIndex] as any)[field] = newValue;
    });

    localStorage.setItem("passengers", JSON.stringify(initialPassengers));

    // Clear state variables
    setPassengers({});
    setInitialPassengers({});
    setPassengerChanges([]);

    // Refresh the page
    window.location.reload();
  };

  // Changes made
  const handleChangesUpdate = (
    changes: Array<{ passengerIndex: number; field: string; newValue: string }>
  ) => {
    const hasNewChanges = changes.some((change) => {
      return !passengerChanges.some((existingChange) => {
        return (
          existingChange.passengerIndex === change.passengerIndex &&
          existingChange.field === change.field &&
          existingChange.newValue === change.newValue
        );
      });
    });

    if (hasNewChanges) {
      const updatedChanges = [...passengerChanges, ...changes];
      setPassengerChanges(updatedChanges);
    }
  };

  const handleChangesMade = (changes: any[]) => {
    setChangesMade(changes.length);
  };

  // Next button enabler / disabler
  const isNextDisabled = () => {
    const hasChanges = Object.keys(passengers).some(
      (key: string) =>
        JSON.stringify(passengers[key]) !==
        JSON.stringify(initialPassengers[key])
    );
    console.log(changesMade);

    return !hasChanges || changesMade > 1;
  };

  return (
    <div>
      <HeaderPage />

      <Stepper
        className="stepper-label"
        activeStep={activeStep}
        alternativeLabel
      >
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>

      {activeStep === 0 ? (
        <PassengerForm
          passengers={passengers}
          onPassengerChange={handlePassengerChange}
          onChangesMade={handleChangesMade}
        />
      ) : (
        <ConfirmationPage
          passengers={passengers}
          initialPassengers={initialPassengers}
          onChangesUpdate={handleChangesUpdate}
        />
      )}
      <div>
        {activeStep !== 0 && (
          <CustomButton
            onClick={handleBack}
            disabled={isNextDisabled()}
            sx={{
              float: "left",
              marginLeft: "22vw",
              width: "15vw",
            }}
          >
            Edit
          </CustomButton>
        )}
        {activeStep !== 1 ? (
          <CustomButton
            onClick={handleNext}
            disabled={isNextDisabled()}
            sx={{
              float: "right",
            }}
          >
            Next
          </CustomButton>
        ) : (
          <CustomButton
            onClick={handleSaveChanges}
            disabled={false}
            sx={{
              float: "right",
              marginRight: "22vw",
              marginTop: "-35px",
              width: "15vw",
            }}
          >
            Submit
          </CustomButton>
        )}
      </div>
    </div>
  );
};

export default App;
