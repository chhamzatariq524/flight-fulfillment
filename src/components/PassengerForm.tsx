import React, { useState, useEffect } from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {
  TextField,
  Grid,
  Card,
  CardHeader,
  CardContent,
  Typography,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import dayjs from "dayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import Avatar from "@mui/material/Avatar";
import PeopleOutlineIcon from "@mui/icons-material/PeopleOutline";

// Passenger props interface
interface PassengerFormProps {
  passengers: any;
  onPassengerChange: (index: number, passenger: any) => void;
  onChangesMade: (changes: any[]) => void;
}

// Initial names interface
interface InitialNameLength {
  [key: number]: {
    firstName: number;
    lastName: number;
  };
}

const PassengerForm: React.FC<PassengerFormProps> = ({
  passengers,
  onPassengerChange,
}) => {
  const [expanded, setExpanded] = useState<string | false>("panel1");
  const [changedField, setChangedField] = useState<null | {
    index: number;
    field: string;
    originalValue: string;
  }>(null);
  const [errorMessages, setErrorMessages] = useState<{ [key: number]: string }>(
    {}
  );

  const [initialNameLength, setInitialNameLength] = useState<InitialNameLength>(
    {}
  );

  // updates when get passengers
  useEffect(() => {
    if (Object.keys(initialNameLength).length === 0) {
      const initialLengths: InitialNameLength = {};

      Object.keys(passengers).forEach((key) => {
        const index = parseInt(key, 10);
        initialLengths[index] = {
          firstName: passengers[index].firstName.length,
          lastName: passengers[index].lastName.length,
        };
      });
      setInitialNameLength(initialLengths);
    }
  }, [passengers]);

  // change handler in fields
  const handleChange = (index: number, field: string, value: string | Date) => {
    // Reset any previous error message
    setErrorMessages((prevErrors) => ({ ...prevErrors, [index]: "" }));

    // Convert Date to string if value is Date (for DOB changes)
    const valueStr =
      value instanceof Date ? value.toISOString().split("T")[0] : value;

    // Check if a different field is being changed after a change was already made
    if (
      changedField &&
      (changedField.index !== index || changedField.field !== field)
    ) {
      setErrorMessages((prevErrors) => ({
        ...prevErrors,
        [index]: "Only one change is allowed per booking.",
      }));
      return;
    }

    // For firstName and lastName, ensure the change is within 3 characters
    if (
      (field === "firstName" || field === "lastName") &&
      Math.abs(initialNameLength[index][field] - valueStr.length) > 3
    ) {
      setErrorMessages((prevErrors) => ({
        ...prevErrors,
        [index]: "You can only change up to 3 characters for name fields.",
      }));
      return;
    }

    // Proceed with updating if the new value is a valid change
    if (!changedField || valueStr === changedField.originalValue) {
      const passenger = { ...passengers[index], [field]: valueStr };
      onPassengerChange(index, passenger);

      // If changing back to the original value, allow changes again
      if (changedField && valueStr === changedField.originalValue) {
        setChangedField(null); // Reset changed field since the original value is restored
      } else if (!changedField) {
        // If no field has been changed yet, track this field change
        setChangedField({
          index,
          field,
          originalValue: passengers[index][field],
        });
      }
    }
  };

  // Handle expansion handler
  const handleExpansionChange =
    (panel: string) => (_: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };

  // Handle date picker
  const handleDateChange = (date: Date | null, index: number) => {
    const formattedDate = date ? dayjs(date).format("YYYY-MM-DD") : "";
    handleChange(index, "dateOfBirth", formattedDate);
  };

  return (
    <Card className="passenger-card">
      <CardHeader
        className="passenger-card-header"
        avatar={
          <Avatar sx={{ bgcolor: "#E3F5FD" }}>
            <PeopleOutlineIcon style={{ color: "black" }} />
          </Avatar>
        }
        title="PASSENGER DETAILS"
      />

      <CardContent>
        <div>
          {Object.keys(passengers).map((_, index: number) => (
            <Accordion
              key={index}
              expanded={expanded === "panel" + (index + 1)}
              onChange={handleExpansionChange("panel" + (index + 1))}
              sx={{ marginBottom: "20px" }}
            >
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls={`panel${index + 1}-content`}
                id={`panel${index + 1}-header`}
              >
                <Typography variant="h6">Passenger {index + 1}</Typography>
              </AccordionSummary>
              <AccordionDetails
                sx={{ flexDirection: "column", marginTop: "auto" }}
              >
                <Grid
                  className="passenger-form"
                  container
                  spacing={2}
                  key={index}
                >
                  <Grid item xs={12}>
                    <Typography
                      variant="subtitle2"
                      sx={{ color: "#092A5E", textAlign: "left" }}
                    >
                      Edit Passenger Details
                    </Typography>

                    <Typography
                      color="error"
                      variant="subtitle2"
                      sx={{ textAlign: "left" }}
                    >
                      {errorMessages[index]}
                    </Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <FormControl
                      variant="standard"
                      sx={{ textAlign: "left" }}
                      fullWidth
                    >
                      <InputLabel id={`title-label-${index}`}>Title</InputLabel>
                      <Select
                        labelId={`title-label-${index}`}
                        id={`title-select-${index}`}
                        value={passengers[index].title}
                        onChange={(e) =>
                          handleChange(index, "title", e.target.value as string)
                        }
                        label="Title"
                      >
                        <MenuItem value="MR">MR</MenuItem>
                        <MenuItem value="MRS">MRS</MenuItem>
                        <MenuItem value="MS">MS</MenuItem>
                        <MenuItem value="MISS">MISS</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl
                      variant="standard"
                      sx={{ textAlign: "left" }}
                      fullWidth
                    >
                      <InputLabel id={`gender-label-${index}`}>
                        Gender
                      </InputLabel>
                      <Select
                        labelId={`gender-label-${index}`}
                        id={`gender-select-${index}`}
                        value={passengers[index].gender}
                        onChange={(e) =>
                          handleChange(
                            index,
                            "gender",
                            e.target.value as string
                          )
                        }
                        label="Gender"
                      >
                        <MenuItem value="MALE">MALE</MenuItem>
                        <MenuItem value="FEMALE">FEMALE</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="First Name"
                      value={passengers[index].firstName}
                      onChange={(e) =>
                        handleChange(index, "firstName", e.target.value)
                      }
                      fullWidth
                      variant="standard"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Last Name"
                      value={passengers[index].lastName}
                      onChange={(e) =>
                        handleChange(index, "lastName", e.target.value)
                      }
                      fullWidth
                      variant="standard"
                    />
                  </Grid>
                  <Grid item xs={12} sx={{ textAlign: "left" }}>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DatePicker
                        variant="standard"
                        sx={{ width: 300 }}
                        label="Date of Birth"
                        value={dayjs(passengers[index].dateOfBirth)}
                        onChange={(date) =>
                          handleDateChange(date?.toDate() ?? null, index)
                        }
                      />
                    </LocalizationProvider>
                  </Grid>
                </Grid>
              </AccordionDetails>
            </Accordion>
          ))}
        </div>
      </CardContent>
    </Card>
  );
};

export default PassengerForm;
