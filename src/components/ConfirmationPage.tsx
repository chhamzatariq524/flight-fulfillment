import React from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Avatar from "@mui/material/Avatar";
import PeopleOutlineIcon from "@mui/icons-material/PeopleOutline";
import { TextField, Grid } from "@mui/material";
import Typography from "@mui/material/Typography";

// Confirmation props interface
interface ConfirmationPageProps {
  passengers: any;
  initialPassengers: any;
  onChangesUpdate: (changes: any[]) => void;
}

const ConfirmationPage: React.FC<ConfirmationPageProps> = ({
  passengers,
  initialPassengers,
  onChangesUpdate,
}) => {
  const changedPassengers = Object.keys(passengers).filter(
    (key: string) =>
      JSON.stringify(passengers[key]) !== JSON.stringify(initialPassengers[key])
  );

  // Get initial values
  const getChanges = () => {
    const changes = [];
    Object.keys(passengers).forEach((key) => {
      const passengerIndex = parseInt(key);
      const currentPassenger = passengers[passengerIndex];
      const initialPassenger = initialPassengers[passengerIndex];
      Object.keys(currentPassenger).forEach((field) => {
        if (currentPassenger[field] !== initialPassenger[field]) {
          changes.push({
            passengerIndex,
            field,
            newValue: currentPassenger[field],
            oldValue: initialPassenger[field],
          });
        }
      });
    });
    return changes;
  };

  const changes = getChanges();

  onChangesUpdate(changes);

  const changeElements = changes.map((change, index) => {
    const isFirstChange =
      index === 0 ||
      change.passengerIndex !== changes[index - 1].passengerIndex;

    return (
      <React.Fragment key={index}>
        {isFirstChange && (
          <Typography
            variant="subtitle2"
            sx={{
              bgcolor: "white",
              color: "#092A5E",
              textAlign: "left",
              padding: "10px",
              marginTop: "10px",
            }}
          >
            Passenger {change.passengerIndex + 1}
          </Typography>
        )}

        <TextField
          label={
            `
            ${change.field}` === "title"
              ? "Title"
              : `${change.field}` === "gender"
              ? "Gender"
              : `${change.field}` === "firstName"
              ? "First Name"
              : `${change.field}` === "lastName"
              ? "Last Name"
              : `${change.field}` === "dateOfBirth"
              ? "Date of Birth"
              : `${change.field}`
          }
          value={`${change.oldValue} → ${change.newValue}`}
          fullWidth
          sx={{
            bgcolor: "#E3F5FD",
            color: "#092A5E",
            textAlign: "left",
            paddingLeft: "0px",
            marginBottom: "1px",
          }}
          inputProps={{
            readOnly: true,
            style: { padding: "10px" },
          }}
          InputLabelProps={{
            style: { padding: "10px" },
          }}
          variant="standard"
        />
      </React.Fragment>
    );
  });

  return (
    <Card className="confirmation-card">
      <CardHeader
        className="confirmation-card-header"
        avatar={
          <Avatar sx={{ bgcolor: "#E3F5FD" }}>
            <PeopleOutlineIcon style={{ color: "black" }} />
          </Avatar>
        }
        title="Confirmation Page"
      />

      <CardContent>
        <div>
          <Grid className="confirmation-review-form" item xs={12}>
            <Typography
              variant="subtitle1"
              sx={{ color: "white", textAlign: "left", marginBottom: "10px" }}
            >
              Review changes you have made*
            </Typography>

            {changeElements}
          </Grid>

          {changedPassengers.map((key: string, index: number) => (
            <Grid
              className="confirmation-form"
              container
              spacing={2}
              key={index}
            >
              <Grid item xs={12}>
                <Typography
                  variant="subtitle2"
                  sx={{ color: "#092A5E", textAlign: "left" }}
                >
                  Passenger {index + 1}
                </Typography>
              </Grid>
              {/* Rest of your grid items */}
              {Object.keys(passengers[key]).map(
                (field: string, fieldIndex: number) => (
                  <Grid item xs={12} key={fieldIndex}>
                    <TextField
                      label={
                        field === "title"
                          ? "Title"
                          : field === "gender"
                          ? "Gender"
                          : field === "firstName"
                          ? "First Name"
                          : field === "lastName"
                          ? "Last Name"
                          : field === "dateOfBirth"
                          ? "Date of Birth"
                          : field
                      }
                      value={passengers[key][field]}
                      sx={{
                        fontWeight: changes.some(
                          (change) => change.field === field
                        )
                          ? "bold"
                          : "normal",
                        bgcolor: changes.some(
                          (change) => change.field === field
                        )
                          ? "#E3F5FD"
                          : "inherit",
                      }}
                      fullWidth
                      InputProps={{
                        readOnly: true,
                      }}
                      variant="standard"
                    />
                  </Grid>
                )
              )}
            </Grid>
          ))}
        </div>
      </CardContent>
    </Card>
  );
};

export default ConfirmationPage;
