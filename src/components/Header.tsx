import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import logo from "../assets/download.png";

export default function HeaderAppBar() {
  return (
    <Box sx={{ display: "flex" }}>
      <AppBar component="nav" sx={{ bgcolor: "#70cbf4" }}>
        <Toolbar>
          <img alt="TUI Logo" width="60" height="30" src={logo} />

          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}
          >
            TUI Flight Fulfillment
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
