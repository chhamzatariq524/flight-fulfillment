# Flight Fulfillment

## Description

Flight Fulfillment is for service center agents to process change requests to the passenger data of a booking

## Installation

1. Clone this repository

2. Navigate to the project directory:

   ```bash
   cd flight_fulfillment
   ```

3. Install dependencies:

   ```bash
   npm install
   ```

5. Run the application:

   ```bash
   npm run dev
   ```
